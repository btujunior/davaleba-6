package com.example.homework6

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_insert.view.*
import kotlinx.android.synthetic.main.activity_insert.view.descriptionEditText
import kotlinx.android.synthetic.main.activity_insert.view.titleEditText
import kotlinx.android.synthetic.main.post_info_layout.*
import kotlinx.android.synthetic.main.post_info_layout.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class PostInfo(p : Post) : AppCompatActivity() {
    private val db: AppDatabase by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()
    }

    var post = p
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.post_info_layout)

        init(post)
    }

    private fun init(post : Post) {
        var activity = MainActivity()
        var oldTitle : String = post.title.toString()
        var oldDescription : String = post.description.toString()

        titleEditText.setText(post.title)
        descriptionEditText.setText(post.description)

        saveInfoButton.setOnClickListener {
            CoroutineScope(IO).launch {
                update(oldTitle, oldDescription)
            }
            activity.openMainActivity()
        }
    }
    private suspend fun update(oldTitle : String, oldDescription : String) {
        db.userDao().update(oldTitle, titleEditText.text.toString(), oldDescription, descriptionEditText.text.toString())
    }
}