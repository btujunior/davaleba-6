package com.example.homework6

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Post {
    @PrimaryKey(autoGenerate = true)
    var pid: Int = 0

    @ColumnInfo(name = "title")
    var title: String? = ""

    @ColumnInfo(name = "description")
    var description: String? = ""
}